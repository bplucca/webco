importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

//const config = {
//  apiKey: "AIzaSyBXfrgjOQUOK5sdwB_cS5bPw72f6wgFIP4",
//  authDomain: "webcolegio-bef0f.firebaseapp.com",
//  databaseURL: "https://webcolegio-bef0f.firebaseio.com",
//  projectId: "webcolegio-bef0f",
//  storageBucket: "webcolegio-bef0f.appspot.com",
//  messagingSenderId: "707254697277",
//  appId: "1:707254697277:web:d976f6f763eee0d7"
//};
//firebase.initializeApp(config);
firebase.initializeApp({
  messagingSenderId: '707254697277'
});
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] ---> Mensaje de servidor recibido', payload);

  const titulo = 'Notificaciones S.G.E.';
  const options = {
    body: payload.data.status,
    icon: '../static/SGEAPP/dist/img/SGE-96.png'
  };

  return self.registration.showNotification(titulo, options);
});



self.addEventListener('push', function (event) {
  console.log('[firebase messaging - Service Worker] Push Received .');
  const title = 'S.G.E. Notificaciones'
  const options = {
    body: event.data.status,
    icon: 'apps/SGEAPP/static/SGEAPP/dist/img/SGE-96.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: 1
    },
    actions: [
      {
        action: 'explore', title: 'Ir al sitio S.G.E.',
        icon: '../img/checkmark.png'
      },
    ]
  };
  event.waitUntil(self.registration.showNotification('Sistema Gestión Educacional', options));
});


// TODO 2.7 - Handle the notificationclick event
self.addEventListener('notificationclick', function (event) {
  console.log('[Service Worker] Notification click Received --------------------------> ');

  // TODO 2.8 - change the code to open a custom page
  const notification = event.notification;
  const primaryKey = notification.data.primaryKey;
  const action = event.action;


  if (action === 'close') {
    notification.close();
    console.log('Closed notification: ' + primaryKey);
  } else {
    event.waitUntil(
      clients.matchAll().then(clis => {
        const client = clis.find(c => {
          return c.visibilityState === 'visible';
        });
        if (client !== undefined) {
          client.navigate('http://localhost:8000/');
          client.focus();
        } else {
          // there are no visible windows. Open one.
          clients.openWindow('http://localhost:8000/');
          notification.close();
        }
      })
    );
  }
  // TODO 5.3 - close all notifications when one is clicked
  self.registration.getNotifications().then(notifications => {
    notifications.forEach(notification => {
      notification.close();
    });
  });
});
