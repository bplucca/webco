importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.2.0/workbox-sw.js');
//importScripts('static/SGEAPP/dist/js/workbox-sw.js');


if (workbox) { //workbox solo existe en el scope del serviceWorker
  console.log('Workbox loaded!');
  //workbox.precaching.precacheAndRoute([]);
} else {
  console.log('Can not load Workbox');
}

/*
Lo primero que vamos a hacer es configurar la cache que vamos a utilizar con Workbox. Workbox puede utilizar dos tipos de cache; La cache estática o precache, y la cache dinámica o de runtime. La configuración de la cache es realmente sencilla, se indica un nombre para estas caches
Como podemos apreciar, contamos con un prefijo, que es el nombre de nuestra aplicación y un sufijo. Este sufijo es realmente útil para evitar conflictos entre las versiones de nuestro service worker y los propios recursos en la cache de tal forma que cuando nuestro service worker sea modificado de forma sustancial, podemos generar una nueva versión de nuestra cache, simplemente cambiando el sufijo.
*/

workbox.core.setCacheNameDetails({
  prefix: 'webco',
  suffix: 'v1',
  precache: 'precache-cache',
  runtime: 'runtime-cache'
});
/*
workbox.precaching.precacheAndRoute([
  '/estudiantes',
  '/cursos',
  '/push'
]);
*/

/*
workbox.routing.registerRoute(
  new RegExp('.*\.js'),
  new workbox.strategies.NetworkFirst({
    cacheName: 'js-cache',
  })
);


workbox.routing.registerRoute(
  /\.css$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'css-cache',
  })
);

workbox.routing.registerRoute(
  /\.(?:png|jpg|jpeg|svg|gif)$/,
  new workbox.strategies.NetworkFirst({
    cacheName: 'image-cache',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 20,
        maxAgeSeconds: 7 * 24 * 60 * 60,
      })
    ],
  })
);
*/

//'use strict';
//listener para notificaciones push
//
//// TODO 3.1 - add push event listener
//self.addEventListener('push', function (event) {
//  console.log('[Service Worker] Push Received.');
//
//  let body;
//  if (event.data) {
//    body = event.data.text();
//  } else {
//    body = 'Default body';
//  }
//  const title = 'S.G.E. Notificaciones'
//  const options = {
//    body: 'Algo ha pasado con su pupilo',
//    icon:  'apps/SGEAPP/static/SGEAPP/dist/img/SGE-96.png',
//    badge: '../img/badge.png',
//    vibrate: [100, 50, 100],
//    data: {
//      dateOfArrival: Date.now(),
//      primaryKey: 1
//    },
//    actions: [
//      {
//        action: 'explore', title: 'Ir al sitio S.G.E.',
//        icon: '../img/checkmark.png'
//      },
//      {
//        action: 'close', title: 'Cerrar notificacion',
//        icon: '../img/xmark.png'
//      },
//    ]
//  };
//
//  //event.waitUntil(self.registration.showNotification('Sistema Gestión Educacional', options));
//  event.waitUntil(
//    clients.matchAll().then(c => {
//      console.log(c);
//      if (c.length === 0) {
//        // Show notification
//        self.registration.showNotification('Sistema Gestion Educacional', options);
//      } else {
//        // Send a message to the page to update the UI
//        console.log('Application is already open!');
//      }
//    })
//  );
//});
//
//



// TODO 2.7 - Handle the notificationclick event
//self.addEventListener('notificationclick', function (event) {
//  console.log('[Service Worker] Notification click Received --------------------------> ');
//
//  // TODO 2.8 - change the code to open a custom page
//  const notification = event.notification;
//  const primaryKey = notification.data.primaryKey;
//  const action = event.action;
//
//
//  if (action === 'close') {
//    notification.close();
//    console.log('Closed notification: ' + primaryKey);
//  } else {
//    event.waitUntil(
//      clients.matchAll().then(clis => {
//        const client = clis.find(c => {
//          return c.visibilityState === 'visible';
//        });
//        if (client !== undefined) {
//          client.navigate('https://b4ckbox.pythonanywhere.com');
//          client.focus();
//        } else {
//          // there are no visible windows. Open one.
//          clients.openWindow('https://b4ckbox.pythonanywhere.com');
//          notification.close();
//        }
//      })
//    );
//  }
//
//  // TODO 5.3 - close all notifications when one is clicked
//  self.registration.getNotifications().then(notifications => {
//    notifications.forEach(notification => {
//      notification.close();
//    });
//  });
//});


// TODO 2.6 - Handle the notificationclose event
//self.addEventListener('notificationclose', event => {
//  const notification = event.notification;
//  const primaryKey = notification.data.primaryKey;
//  console.log('Closed notification: ' + primaryKey);
//});
//












/*
function showNotification(event) {
  return new Promise(resolve => {
    const { body, title, tag } = JSON.parse(event.data.text());

    self.registration
      .getNotifications({ tag })
      .then(existingNotifications => { })
      .then(() => {
        const icon = '../img/icon.png';
        return self.registration
          .showNotification(title, { body, tag, icon })
      })
      .then(resolve)
  })
}

self.addEventListener("push", event => {
  event.waitUntil(showNotification(event));
});

self.addEventListener("notificationclick", event => {
  event.waitUntil(clients.openWindow("http://localhost:8000/perfil"));
});


*/

















/*


workbox.routing.registerRoute(
  '/',
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'rutas',
  })
);



workbox.routing.registerNavigationRoute(
  workbox.precaching.getCacheKeyForURL('/single'),{
    whitelist: [
      new RegExp('/\/')
    ],
    blacklist: [
      new RegExp('/admin/'),
    ]
});



Probablemente uno de los métodos más útiles de Workbox es el método registerRoute del módulo routing que nos permite registrar rutas y actuar en consecuencia cuando se realice algún tipo de petición a dicha ruta. Estas rutas no tienen por qué ser strings fijos, si no que pueden ser expresiones regulares que nos permitan definir un amplio rango de posibilidades. Vamos a imaginar que queremos añadir a la cache estática que hemos definido al principio, todos los archivos js y css de nuestra PWA.

workbox.routing.registerRoute(
  /\.(?:js|css)$/, //todos los archivos js y css
  workbox.strategies.cacheFirst({
    cacheName: workbox.core.cacheNames.precache, //nombre de la cache donde queremos guardar el recurso
  })
);



Cuando registremos una url (en este caso todos los recursos del directorio content), usando la estrategia cacheFirst, lo que haremos será asegurarnos, que, si ese recurso ya existe en la cache, por que el usuario ya visito nuestra PWA y el recurso se encuentra ya en la cache, este será servido desde la propia cache antes de realizar una petición al recurso, lo cual es notablemente más lento por los tiempos de red.

workbox.routing.registerRoute(
  /\./,
  workbox.strategies.cacheFirst()
);





Otra de las estrategias más utilizas. Funciona de forma similar a cacheFirst pero con la diferencia, de que, stateWhileRevalidate sirve el contenido de la cache, si este se encuentra en la cache, pero adicionalmente, revalida la propia cache con una petición al recurso para obtener la versión más actual del mismo y que volverá a ser servida desde la cache en la siguiente conexion

workbox.routing.registerRoute(
  /perfil/,
  workbox.strategies.staleWhileRevalidate()
);


El precaching. es una de funcionalidades principales de Worbox y está totalmente orientada al cacheo estático. La ventaja del precaching es que permite el cacheo de determinados recursos antes incluso de que el service worker haya sido instalado (no confundir con registrado). Para ello, necesitamos saber desde un primer momento, que archivos son los que queremos cachear y su mejor ejemplo de uso es precisamente, el soporte Offline, donde vamos a precachear todos aquellos recursos de los que queremos disponer sin conexion.

workbox.routing.registerRoute(
  '/',
  workbox.strategies.cacheFirst()
);




Otra de las grandes ventajas de Workbox es la posibilidad de emplear plugins que extiendan la funcionalidad, pudiendo controlar el tiempo de expiración de los recursos o incluso cuantos recursos queremos cachear, algo que por ejemplo en sites muy grandes donde en un mismo directorio se sirven una cantidad grande de recursos puede requerir:
En el ejemplo estamos utilizando el expiration plugin, uno de los diversos plugins que vienen con Workbox y que nos permite definir un número máximo de elementos para la ruta ´content´con lo cual solo cachearía los 60 primeros y un maxAge de 30 días

workbox.routing.registerRoute(
  new RegExp('/perfil/(.*)'),
  workbox.strategies.staleWhileRevalidate({
    cacheName: workbox.core.cacheNames.runtime,
    plugins: [
      new workbox.expirations.Plugin({
        maxEntries: 60, //limitamos a 60 el numero de recursos que queremos cachear
        maxAgeSeconds: 30*24*60*60 //tiempo de vida de la cache 30 dias
      })
    ]
  })
);





Otra de las grandes funcionalidades que podemos abordar con Workbox es la sincronización en segundo plano mediante el uso de SyncManager que nos permite registrar eventos ´sync´. Estos eventos son útiles, cuando por ejemplo, queremos tener la plena certeza de enviar una determinada petición al servidor, al margen de que haya conexión o no, ya que lo que esperamos es que si la conexión se pierde, en el momento que esta vuelva se realicen las operaciones que hemos registrado previamente. Para ello contaremos con una cola a la que indicaremos el tiempo durante el cual se realizan los reintentos de sincronización:

const syncHandler = new workbox.backgroundSync.Plugin('syncQueue',{
  maxRetentionTime: 12*60 // duracion maxima de la sincronizacion
});

workbox.routing.registerRoute(
  /api/,
  workbox.strategies.networkOnly({
    plugins: [syncHandler] // añadimos el plugin
  }),
  'POST'
);




self.addEventListener('install', function (e) {
  console.log('[Service-Worker] Instalando');
  e.waitUntil(
    caches.open(cacheName).then(function (cache) {
      console.log('[Service-Worker] Almacenando shell en el cache')
      return cache.addAll(filesToCache);
    })
  );
});



self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        if (response) {
          console.log('[Service Worker] respuesta a solicitud')
          return response;
        }
        var fetchRequest = event.request.clone();
        return fetch(fetchRequest).then(
          function (response) {
            if (!response || response.status !== 200 || response.type !== 'basic') {
              return response;
            }
            var responseToCache = response.clone();
            caches.open(cacheName)
              .then(function (cache) {
                cache.put(event.request, responseToCache);
              });
            return response;
          }
        );
      })
  );
});



self.addEventListener('activate', function (e) {
  e.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function (key) {
        if (key !== cacheName) {
          return caches.delete(key);
        }
      }));
    })
  );
});
*/
