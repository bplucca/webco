const config = {
    apiKey: "AIzaSyBXfrgjOQUOK5sdwB_cS5bPw72f6wgFIP4",
    authDomain: "webcolegio-bef0f.firebaseapp.com",
    databaseURL: "https://webcolegio-bef0f.firebaseio.com",
    projectId: "webcolegio-bef0f",
    storageBucket: "webcolegio-bef0f.appspot.com",
    messagingSenderId: "707254697277",
    appId: "1:707254697277:web:d976f6f763eee0d7"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();
const database = firebase.database();
var referencia = database.ref('usuarios');

messaging.usePublicVapidKey("BBzM7wa26RcQ0NdeAEKtu4dicdlcUpfOh1eoFnkkzv1dffQJXcRwxEUIfl_U9L9vxg7FtSbzCC9zkShAg6PjfuU");

messaging.onMessage(function (payload) {
    console.log('[main.js] ---> onMessage ---> ', payload);
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "8000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"

    }
    toastr["warning"](payload.notification.body, payload.notification.title);
});




messaging.requestPermission()
    .then(function () {
        console.log('[main js] ---> permiso concedido');
        return messaging.getToken();
    })
    .then(function (token) {
        console.log(token);
        sendTokenToServer(token);
    })
    .catch(function (err) {
        console.log('[main js] ---> Error', err);
    });



messaging.getToken().then(function (currentToken) {
    if (currentToken) {
        sendTokenToServer(currentToken);
    } else {
        console.log('[main js] ---> No hay una instancia de token disponible. Solicite permisos para generar una.');
        setTokenSentToServer(false);
    }
}).catch(function (err) {
    console.log('[main js] ---> Ha ocurrido un error mientras se recibia el token. ---> ', err);
    setTokenSentToServer(false);
});


function sendTokenToServer(currentToken) {

    if (!isTokenSentToServer()) {
        console.log('Sending token to server...');

        var objeto = {
            idusuario: idUserActual,
            app: "webcolegio",
            token: currentToken
        };

        referencia.set(objeto)
            .then(function () {
                console.log('[main js] ---> Token almacenado');
                saveToken(currentToken);
            })
            .catch(function (err) {
                console.log('[main js] ---> Error al guardar el token ---> ', err);
            });

        setTokenSentToServer(true);
    } else {
        console.log('[main js] ---> El token ya fue enviado al servidor, no se volvera a enviar hasta que cambie.');
    }

}


function setTokenSentToServer(sent) {
    if (sent) {
        window.localStorage.setItem('sentToServer', 1);
    } else {
        window.localStorage.setItem('sentToServer', 0);
    }
}

messaging.onTokenRefresh(function () {
    messaging.getToken()
        .then(function (refreshedToken) {
            console.log('[main js] ---> Token refreshed.');
            setTokenSentToServer(false);
            sendTokenToServer(refreshedToken);
            resetUI();
        })
        .catch(function (err) {
            console.log('[main js] ---> Unable to retrieve refreshed token ', err);
            showToken('[main js] ---> Unable to retrieve refreshed token ', err);
        });
});

function isTokenSentToServer() {
    if (window.localStorage.getItem('sentToServer') == 1) {
        return true;
    }
    return false;
}


function saveToken(currentToken) {
    console.log('[main js] ---> guardando token en la base de datos');
    var token = {
        'currentToken': currentToken,
        'user': idUserActual
    };
    $.post(url, token, function (response) {
        if (response === 'success') {
            console.log('[main js] ---> Token enviado al backend');
        } else {
            console.log('[main js] ---> Error al enviar token al backend');
        }
    });
}
