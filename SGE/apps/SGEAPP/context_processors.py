from .models import *
from django.shortcuts import render, redirect

def data_templates(request):
    try:
        current_user = request.user
        perfil = Perfil.objects.get(usuario=current_user) 
    except:
        current_user = False
        perfil = False
    cursos_gen = Curso.objects.all()
    secciones_gen = Seccion.objects.all()
    g_session = False
    pupilos = False
    if current_user != False:
        if perfil.tipo_usuario == "Estudiante":
            g_session = Estudiante.objects.get(rut=perfil.rut)
        elif perfil.tipo_usuario == "Profesor":
            g_session = Profesor.objects.get(rut=perfil.rut)
            cursos_gen = Curso.objects.filter(profesor = g_session)
        elif perfil.tipo_usuario == "Apoderado":
            g_session = Apoderado.objects.get(rut=perfil.rut)
            pupilos = Estudiante.objects.filter(apoderado=g_session)
    try:
        notificaciones = Mensaje.objects.filter(perfil=perfil)
        cant_not = len(notificaciones)
    except:
        notificaciones = False
        cant_not = 'x'
    return {'current_user':current_user,'perfil':perfil, 'cursos_gen':cursos_gen, 'secciones_gen':secciones_gen, 'g_session':g_session, 'pupilos':pupilos, 'cant_not':cant_not, 'notificaciones':notificaciones}