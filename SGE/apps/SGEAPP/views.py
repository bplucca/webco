from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.views.generic import CreateView
from django.views import View

from .models import *
from .forms import *
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST

from .render import Render

from django.conf import settings

from django.core.mail import send_mail
from django.contrib import messages

import requests
import urllib.request
import time
from bs4 import BeautifulSoup
from lxml import html

from unicodedata import normalize
import sys
import datetime

from django.contrib.auth.decorators import login_required

from django_xhtml2pdf.utils import pdf_decorator
# Create your views here.
def login_fn(request):
    if request.method == 'POST':
        form = AuthenticationForm(request.POST)
        username = request.POST['username'].upper()
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('index')
        else:
            messages.error(request,'Usuario o contraseña incorrecto')
            return redirect('login_fn')

    else:
        form = AuthenticationForm()
    context = {'form' : form}
    return render(request, 'SGEAPP/login.html', context)

def index(request):
    return render(request, 'SGEAPP/index.html', {})

def usuarios(request):
    usuarios = User.objects.all()
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('usuarios')
    else:
        form = SignUpForm()
    contexto = {'usuarios':usuarios, 'form':form, 'nbar':'Usuarios'}
    return render(request, 'SGEAPP/temp_usuarios.html', contexto)

def crear_cuentas_profesores(request):
    with requests.Session() as s:
        headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
        }
        login_data = {
            'accion': 'login',
            'usuario': '1506',
            'dv':'', 
            'clave': 'florida2017',
            'perfil': '1'   
        }
        url = "https://sige.mineduc.cl/Sige/Login"
        r = s.get(url, headers=headers)
        soup = BeautifulSoup(r.content, 'html')
        r = s.post(url, data=login_data, headers=headers)
        url2 = "https://sige.mineduc.cl/Sige/FichaEstablecimiento/MantDocente"
        x = s.post(url2, headers=headers)
        soup2 = BeautifulSoup(x.content)
        profesores = soup2.find_all("td")
        x = 1
        largo = len(profesores)
        lista = []
        while x < largo:
            profe = Profesor.objects.create()
            profe.rut = profesores[x].text
            profe.rut = profe.rut.replace(".","")
            profe.nom = profesores[x+1].text.split()[2]
            profe.apelp = profesores[x+1].text.split()[0]
            profe.apelm = profesores[x+1].text.split()[1]
            profe.save()
            lista.append(profe)
            x = x+8
        for prof in lista:
            try:
                usrnm = prof.nom+prof.apelp
                trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
                usrnm = normalize('NFKC', normalize('NFKD', usrnm).translate(trans_tab))
                user = User.objects.create_user(username=usrnm, password=prof.rut, last_name=prof.apelp, first_name=prof.nom)
                user.set_password('florida2018')
                user.save()
                perfil = Perfil.objects.get(usuario=user)
                perfil.rut = prof.rut
                perfil.tipo_usuario = "Profesor"
                perfil.save()
            except:
                False
    return redirect('usuarios')

def crear_cuentas_estudiantes(request):
    class Curso_m:
        num = ""
        letra = ""
        tipo_ensenanza = ""
        grado = ""
    # Comienzo entregando al información que debo entregar para el login que es la que sale en form data en el navegador
    with requests.Session() as s:
        headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
        }
        login_data = {
            'accion': 'login',
            'usuario': '1506',
            'dv':'', 
            'clave': 'florida2017',
            'perfil': '1'   
        }
        # Entrego la dirección del login
        url = "https://sige.mineduc.cl/Sige/Login"
        # Entro a la url
        r = s.get(url, headers=headers)
        # Desde ahí mando un post con el formulario lleno
        r = s.post(url, data=login_data, headers=headers)
        # Luego en la misma sesión me dirigo a la dirección ddonde están los cursos, puedo entrar porque ya pasé el login
        url2 = "https://sige.mineduc.cl/Sige/Matricula/DespliegaEstructura"
        # Mando el html a la variable X
        x = s.post(url2, headers=headers)
        # Creo un objeto de beautiful soup con el contenido de X
        soup2 = BeautifulSoup(x.content)
        # Creo un arreglo que se llama lista_tipos donde guardaré los tipos de enseñanza registrados
        lista_tipos = []
        lista_cursos = []
        tablas = soup2.find_all("table")
        for tbl in tablas:
            tipo = tbl.find_all("th")
            tipo_n = tipo[0].text.split()[0]
            try:
                TipoEnsenanza.objects.create(cod=tipo_n,nom=tipo[0].text.replace(str(tipo_n),""))
            except Exception as e:
                messages.error(request,str(e))
                break
            cursos = tbl.find_all("td")
            for i in cursos:
                try:
                    # Creo un objeto curso
                    curso = Curso_m()
                    curso.tipo_ensenanza = tipo_n
                    nombre = i.text
                    l = len(nombre)
                    curso.letra = nombre[0:l-6].split()[-1]
                    curso.num = nombre[0:l-6].split()[0].replace("°","")
                    curso.num = curso.num.replace("er","")
                    curso.num = curso.num.replace("º","")
                    te = TipoEnsenanza.objects.get(cod=tipo_n)
                    try:
                        gr = Grado.objects.create(nombre=nombre[0:l-8],tipoensenanza_cod=te)
                    except:
                        gr = Grado.objects.get(nombre=nombre[0:l-8],tipoensenanza_cod=te)
                    curso.grado = gr
                    lista_cursos.append(curso)
                except Exception as e:
                    messages.error(request,str(e))
                    break
        largo_cursos = len(lista_cursos)
        lista = []
        for cur in lista_cursos:
            Curso.objects.create(codigo=str(cur.num+cur.letra+cur.tipo_ensenanza), letra=cur.letra, grado=cur.grado)
            form_data = {
                'txtNombEstab': '',
                'txtTipoense': cur.tipo_ensenanza,
                'txtCurso': cur.num,
                'txtLetra': cur.letra,
                'txtCursoAutorizado': '1'
            }
            url3 = "https://sige.mineduc.cl/Sige/Matricula/DespliegaCurso"
            z = s.post(url3, headers=headers, data=form_data)
            soup3 = BeautifulSoup(z.content)
            estudiantes = soup3.find_all("td")
            x = 6
            for i in estudiantes:
                estud = Estudiante()
                try:
                    estud.nombre = estudiantes[x].text.upper()
                    estud.rut = estudiantes[x-1].text
                    estud.rut = estud.rut.replace(".","")
                    usrnm = estud.nombre.split()[2]+estud.nombre.split()[0]
                    trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
                    usrnm = normalize('NFKC', normalize('NFKD', usrnm).translate(trans_tab))
                    user = User.objects.create_user(username=usrnm, password=estud.rut, last_name=estud.nombre.split()[0], first_name=estud.nombre.split()[2])
                    user.set_password('florida2018')
                    user.save()
                    perfil = Perfil.objects.get(usuario=user)
                    perfil.rut = estud.rut
                    perfil.tipo_usuario = "Estudiante"
                    perfil.save()
                    rutx = str(estud.rut)
                    cur_estud = Curso.objects.get(codigo=str(cur.num+cur.letra+cur.tipo_ensenanza))
                    try:
                        Estudiante.objects.create(rut=rutx, nom=user.first_name, apelp=user.last_name, apelm=estud.nombre.split()[1], curso=cur_estud)
                    except Exception as e:
                        messages.error(request,str(e))
                        break
                except:
                    False
                x = x+9
            largo = len(lista)
    return redirect('usuarios')

def crear_cuentas_funcionarios(request):
    with requests.Session() as s:
        headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
        }
        login_data = {
            'accion': 'login',
            'usuario': '1506',
            'dv':'', 
            'clave': 'florida2017',
            'perfil': '1'   
        }
        url = "https://sige.mineduc.cl/Sige/Login"
        r = s.get(url, headers=headers)
        soup = BeautifulSoup(r.content, 'html')
        r = s.post(url, data=login_data, headers=headers)
        url2 = "https://sige.mineduc.cl/Sige/FichaEstablecimiento/MantAsistente"
        x = s.post(url2, headers=headers)
        soup2 = BeautifulSoup(x.content)
        funcionarios = soup2.find_all("td")
        x = 1
        largo = len(funcionarios)
        lista = []
        while x < largo:
            fun = Funcionario.objects.create()
            fun.rut = funcionarios[x].text
            fun.rut = fun.rut.replace(".","")
            fun.nom = funcionarios[x+1].text.split()[2]
            fun.apelp = funcionarios[x+1].text.split()[0]
            fun.apelm = funcionarios[x+1].text.split()[1]
            fun.save()
            lista.append(fun)
            x = x+8
        for func in lista:
            try:
                usrnm = func.nom+func.apelp
                trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
                usrnm = normalize('NFKC', normalize('NFKD', usrnm).translate(trans_tab))
                user = User.objects.create_user(username=usrnm, password=func.rut, last_name=func.apelp, first_name=func.nom)
                user.set_password('florida2018')
                user.save()
                perfil = Perfil.objects.get(usuario=user)
                perfil.rut = func.rut
                perfil.tipo_usuario = "Funcionario"
                perfil.save()
            except:
                False
    return redirect('usuarios')

def editUsuario(request, id_usuario):
    usuario = User.objects.get(id = id_usuario)
    if request.method == 'GET':
        form = UsuarioForm(instance=usuario)
    else:
        form = UsuarioForm(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
        return redirect('usuarios')
    return render(request,'SGEAPP/editUsuario.html',{'form':form})

def delUsuario(request,id_usuario):
    usuario = User.objects.get(id=id_usuario)
    usuario.delete()
    return redirect ('usuarios')

def cleanUsuario(request):
    usuarios = User.objects.all()
    for i in usuarios:
        if i.is_staff:
            continue
        else:
            i.delete()
    return redirect ('usuarios')
    
def perfiles(request):
    perfiles = Perfil.objects.all()
    if request.method == 'POST':
        form = PerfilForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('perfiles')
    else:
        form = PerfilForm()
    contexto = {'perfiles':perfiles, 'form':form, 'nbar':'Perfiles'}
    return render(request, 'SGEAPP/temp_perfiles.html', contexto)

def delPerfil(request,id_perfil):
    perfil = Perfil.objects.get(id=id_perfil)
    perfil.delete()
    return redirect ('perfiles')

def editPerfil(request, id_perfil):
    perfil = Perfil.objects.get(id = id_perfil)
    if request.method == 'GET':
        form = PerfilForm(instance=perfil)
    else:
        form = PerfilForm(request.POST, request.FILES, instance=perfil)
        if form.is_valid():
            form.save()
        return redirect('perfiles')
    return render(request,'SGEAPP/editPerfil.html',{'form':form})

def perfil(request):
    current_user = request.user
    perfil = Perfil.objects.get(usuario = current_user)
    return render(request, 'SGEAPP/perfil.html', {'perfil':perfil})

def editUser(request,id_user):
    current_user = request.user
    if int(current_user.id) == int(id_user) or current_user.is_staff:
        print(current_user.id)
        user = User.objects.get(id=id_user)
        if request.method == 'GET':
            form = EditUserForm(instance=user)
        else:
            form = EditUserForm(request.POST, instance=user)
            if form.is_valid():
                form.save()
            return redirect('index')
    else:
        return redirect ('index')
    return render(request, 'SGEAPP/editUser.html',{'form':form})

def listAsistenciaFilter(request, curso_id, mes):
    ## Creando asistencia de un día
    if request.method == 'POST':
        formasis = AsistenciaForm(request.POST)
        if formasis.is_valid():
            formasis.save()
            cursopost = Curso.objects.get(id=request.POST.get('curso'))
            fechapost = request.POST.get('fecha')
            estudiantes = Estudiante.objects.filter(curso=cursopost)
            for est in estudiantes:
                Asistencia.objects.create(curso=cursopost, estudiante=est, fecha=request.POST.get('fecha'))
        return redirect('listAsistenciaFilter', curso_id=curso_id, mes=mes)
    else:
        formasis = AsistenciaForm()
    ## Fin de crear asistencia
    estudiante = Estudiante.objects.filter(curso_id=curso_id)
    list_curso = Curso.objects.all()
    id_cur = Curso.objects.get(id=curso_id)
    asistencia = Asistencia.objects.all()
    nmes = mes
    dias = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
    if request.method == 'GET':
        contexto = {'estudiantes':estudiante,'list_curso':list_curso,'id_cur':id_cur, 'asistencias':asistencia, 'nmes':nmes, 'dias':dias, 'formasis':formasis}
    else:
        idasistencia = request.GET.get('id')
        asistencianew = Asistencia.objects.get(id=idasistencia)
        form = AsistenciaMinForm(request.GET, instance=asistencianew)
        if form.is_valid():
            form.save()
            contexto= {'form':form, 'formasis':formasis}
            return redirect('listAsistenciaFilter', curso_id=curso_id)
        else:
            idasistencia = request.GET.get('id')
            asistencianew = Asistencia.objects.get(id=idasistencia)
            form = AsistenciaMinForm(request.GET, instance=asistencianew)
            contexto= {'formasis':formasis}

    return render(request, 'SGEAPP/listAsistenciaFilter.html', contexto)

def crearAsistencia(request,id_curso, fecha):
    curso = Curso.objects.get(id=id_curso)
    estudiantes = Estudiante.objects.all().filter(curso=curso)
    for est in estudiantes:
        Asistencia.objects.create(curso=curso, fecha=fecha, estudiante=est)
    return redirect('listAsistenciaFilter', curso_id=id_curso, mes=1)

def crearAsistenciaMes(request,id_curso, mes):
    try:   
        curso = Curso.objects.get(id=id_curso)
        estudiantes = Estudiante.objects.all().filter(curso=curso)
        for est in estudiantes:
            i = 1
            if mes == "1" or mes == "3" or mes == "5" or mes == "7" or mes == "9" or mes =="11":
                while i < 31:
                    fecha = datetime.datetime(2018, int(mes), int(i))
                    try:
                        Asistencia.objects.create(curso=curso, fecha=fecha, estudiante=est)
                    except Exception as e:
                        messages.error(request,str(e))
                        break
                    i = i+1
            else:
                while i < 30:
                    fecha = datetime.datetime(2018, int(mes), int(i))
                    Asistencia.objects.create(curso=curso, fecha=fecha, estudiante=est)
                    i = i+1
        return redirect('listAsistenciaFilter', curso_id=id_curso, mes=mes)
    except Exception as e:
        messages.error(request,str(e))
        return redirect('listAsistenciaFilter', curso_id=id_curso, mes=mes)

def editAsistencia(request,id_asistencia,id_curso,mes):
    asistencia = Asistencia.objects.get(id=id_asistencia)
    estado = asistencia.estado
    if estado:
        asistencia.estado = False
        asistencia.save()
    else:
        asistencia.estado = True
        asistencia.save()
    return redirect('listAsistenciaFilter', curso_id=id_curso, mes=mes)

def grados(request):
    grados = Grado.objects.all()
    if request.method == 'POST':
        form = GradoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('grados')
    else:
        form = GradoForm()
    contexto = {'grados':grados, 'form':form}
    return render(request, 'SGEAPP/temp_grados.html', contexto)

def delGrado(request,id_grado):
    grado = Grado.objects.get(id=id_grado)
    grado.delete()
    return redirect ('grados')

def tiposEnsenanza(request):
    tipoensenanza = TipoEnsenanza.objects.all()
    if request.method == 'POST':
        form = TipoEnsenanzaForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('tiposEnsenanza')
    else:
        form = TipoEnsenanzaForm()
    contexto = {'tipos':tipoensenanza, 'form':form}
    return render(request, 'SGEAPP/TiposDeEnsenanza.html', contexto)

def delTipoEnsenanza(request,id_tipoensenanza):
    tipoensenanza = TipoEnsenanza.objects.get(id=id_tipoensenanza)
    tipoensenanza.delete()
    return redirect ('tiposEnsenanza')

def v_profesores(request):
    profesores = Profesor.objects.all()
    if request.method == 'POST':
        form = ProfesorForm(request.POST)
        if form.is_valid():
            form.save()
            password = request.POST['rut']
            username = request.POST['nom']+request.POST['apelp']
            first_name = request.POST['nom']
            last_name = request.POST['apelp']
            mail = request.POST['mail']
            usuario = User.objects.create(password=password, username = username, first_name=first_name, last_name=last_name, email=mail)
            usuario.set_password(password)
            usuario.save()
            perfil = Perfil.objects.get(usuario = usuario)
            perfil.rut = request.POST['rut']
            perfil.tipo_usuario = 'Profesor'
            perfil.save()
            subject = first_name+' '+last_name+' BIENVENIDO A SGE!'
            message = ' Ya eres parte de nuestra comunidad! Tu usuario es '+usuario.username+' y tu clave es tu rut con el siguiente formato ej: 11111111-1. INGRESA AQUÍ: https://b4ckbox.pythonanywhere.com'
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [mail,]
            send_mail( subject, message, email_from, recipient_list )
        return redirect('profesores')
    else:
        form = ProfesorForm()
    contexto = {'profesores':profesores, 'form':form}
    return render(request, 'SGEAPP/temp_profesores.html', contexto)

def editProfesor(request, id_profesor):
    profesor = Profesor.objects.get(id = id_profesor)
    if request.method == 'GET':
        form = ProfesorForm(instance=profesor)
    else:
        form = ProfesorForm(request.POST, instance=profesor)
        if form.is_valid():
            form.save()
        return redirect('profesores')
    return render(request,'SGEAPP/editProfesor.html',{'form':form})

def delProfesor(request,id_profesor):
    profesor = Profesor.objects.get(id=id_profesor)
    profesor.delete()
    return redirect ('profesores')

def v_cursos(request):
    cursos = Curso.objects.all()
    if request.method == 'POST':
        form = CursoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('cursos')
    else:
        form = CursoForm()
    contexto = {'cursos':cursos, 'form':form}
    return render(request, 'SGEAPP/temp_cursos.html', contexto)

def editCurso(request, id_curso):
    curso = Curso.objects.get(id = id_curso)
    if request.method == 'GET':
        form = CursoForm(instance=curso)
    else:
        form = CursoForm(request.POST, instance=curso)
        if form.is_valid():
            form.save()
        return redirect('cursos')
    return render(request,'SGEAPP/editCurso.html',{'form':form})

def delCurso(request):
    curso = Curso.objects.get(id=id_curso)
    curso.delete()
    return redirect ('cursos')

def v_tipoperfiles(request):
    tipoperfiles = TipoPerfil.objects.all()
    if request.method == 'POST':
        form = TipoPerfilForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('tipoperfiles')
    else:
        form = TipoPerfilForm()
    contexto = {'tipoperfiles':tipoperfiles, 'form':form}
    return render(request, 'SGEAPP/temp_tipoperfil.html', contexto)

def delTipoPerfil(request,id_tipoperfil):
    tipoperfil = TipoPerfil.objects.get(id=id_tipoperfil)
    tipoperfil.delete()
    return redirect ('tipoperfiles')


def v_estudiantes(request):
    estudiantes = Estudiante.objects.all()
    if request.method == 'POST':
        form = EstudianteForm(request.POST)
        if form.is_valid():
            form.save()
            password = request.POST['rut']
            username = request.POST['nom']+request.POST['apelp']
            first_name = request.POST['nom']
            last_name = request.POST['apelp']
            mail = request.POST['mail']
            usuario = User.objects.create(password=password, username = username, first_name=first_name, last_name=last_name, email=mail)
            usuario.set_password(password)
            usuario.save()
            perfil = Perfil.objects.get(usuario = usuario)
            perfil.rut = request.POST['rut']
            perfil.tipo_usuario = 'Estudiante'
            perfil.save()
            subject = first_name+' '+last_name+' BIENVENIDO A SGE!'
            message = ' Ya eres parte de nuestra comunidad! Tu usuario es '+usuario.username+' y tu clave es tu rut con el siguiente formato ej: 11111111-1. INGRESA AQUÍ: https://b4ckbox.pythonanywhere.com'
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [mail,]
            send_mail( subject, message, email_from, recipient_list )
        return redirect('estudiantes')
    else:
        form = EstudianteForm()
    contexto = {'estudiantes':estudiantes, 'form':form}
    return render(request, 'SGEAPP/temp_estudiantes.html', contexto)

def editEstudiante(request, rut):
    estudiante = Estudiante.objects.get(rut = rut)
    if request.method == 'GET':
        form = EstudianteForm(instance=estudiante)
    else:
        form = EstudianteForm(request.POST, instance=estudiante)
        if form.is_valid():
            form.save()
        return redirect('estudiantes')
    return render(request,'SGEAPP/editEstudiante.html',{'form':form})

def delEstudiante(request,rut):
    estudiante = Estudiante.objects.get(rut=rut)
    estudiante.delete()
    return redirect ('estudiantes')

def v_funcionarios(request):
    funcionarios = Funcionario.objects.all()
    if request.method == 'POST':
        form = FuncionarioForm(request.POST)
        if form.is_valid():
            form.save()
            password = request.POST['rut']
            username = request.POST['nom']+request.POST['apelp']
            first_name = request.POST['nom']
            last_name = request.POST['apelp']
            mail = request.POST['mail']
            usuario = User.objects.create(password=password, username = username, first_name=first_name, last_name=last_name, email=mail)
            usuario.set_password(password)
            usuario.save()
            perfil = Perfil.objects.get(usuario = usuario)
            perfil.rut = request.POST['rut']
            perfil.tipo_usuario = 'Funcionario'
            perfil.save()
            subject = first_name+' '+last_name+' BIENVENIDO A SGE!'
            message = ' Ya eres parte de nuestra comunidad! Tu usuario es '+usuario.username+' y tu clave es tu rut con el siguiente formato ej: 11111111-1 INGRESA AQUÍ: https://b4ckbox.pythonanywhere.com'
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [mail,]
            try:
                send_mail( subject, message, email_from, recipient_list )
            except Exception as e:
                messages.error(request,str(e))
        return redirect('funcionarios')
    else:
        form = FuncionarioForm()
    contexto = {'funcionarios':funcionarios, 'form':form}
    return render(request, 'SGEAPP/temp_funcionarios.html', contexto)

def editFuncionario(request, rut):
    funcionario = Funcionario.objects.get(rut = rut)
    if request.method == 'GET':
        form = FuncionarioForm(instance=funcionario)
    else:
        form = FuncionarioForm(request.POST, instance=funcionario)
        if form.is_valid():
            form.save()
        return redirect('funcionarios')
    return render(request,'SGEAPP/editFuncionario.html',{'form':form})

def delFuncionario(request,rut):
    funcionario = Funcionario.objects.get(rut=rut)
    funcionario.delete()
    return redirect ('funcionarios')

def v_apoderados(request):
    apoderados = Apoderado.objects.all()
    if request.method == 'POST':
        form = ApoderadoForm(request.POST)
        if form.is_valid():
            form.save()
            password = request.POST['rut']
            username = request.POST['nom']+request.POST['apelp']
            first_name = request.POST['nom']
            last_name = request.POST['apelp']
            mail = request.POST['mail']
            usuario = User.objects.create(password=password, username = username, first_name=first_name, last_name=last_name, email=mail)
            usuario.set_password(password)
            usuario.save()
            perfil = Perfil.objects.get(usuario = usuario)
            perfil.rut = request.POST['rut']
            perfil.tipo_usuario = 'Apoderado'
            perfil.save()
            subject = first_name+' '+last_name+' BIENVENIDO A SGE!'
            message = ' Ya eres parte de nuestra comunidad! Tu usuario es '+usuario.username+' y tu clave es tu rut con el siguiente formato ej: 11111111-1. INGRESA AQUÍ: https://b4ckbox.pythonanywhere.com'
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [mail,]

            send_mail( subject, message, email_from, recipient_list )
        return redirect('apoderados')
    else:
        form = ApoderadoForm()
    contexto = {'apoderados':apoderados, 'form':form}
    return render(request, 'SGEAPP/temp_apoderados.html', contexto)

def editApoderado(request, rut):
    apoderado = Apoderado.objects.get(rut = rut)
    if request.method == 'GET':
        form = ApoderadoForm(instance=apoderado)
    else:
        form = ApoderadoForm(request.POST, instance=apoderado)
        if form.is_valid():
            form.save()
        return redirect('apoderados')
    return render(request,'SGEAPP/editApoderado.html',{'form':form})

def delApoderado(request,rut):
    apoderado = Apoderado.objects.get(rut=rut)
    perfil = Perfil.objects.get(rut=rut)
    apoderado.delete()
    sendPush(perfil.token)
    return redirect ('apoderados')

def v_secciones(request):
    secciones = Seccion.objects.all()
    cursos = Curso.objects.all()
    asignaturas = Asignatura.objects.all()
    pfs = Profesor.objects.all()
    if request.method == 'POST':
        form = SeccionForm(request.POST)
        if form.is_valid():
            Seccion.objects.create( cod_seccion= Asignatura.objects.get(id=request.POST['asignatura']).cod_asign+Curso.objects.get(id=request.POST['curso']).codigo[0:2]+'19'.upper(),
                                    curso= Curso.objects.get(id=request.POST['curso']),
                                    asignatura= Asignatura.objects.get(id=request.POST['asignatura']),
                                    profesor=Curso.objects.get(id=request.POST['curso']).profesor
                                    #profesor=Profesor.objects.get(id=request.POST['profesor']) 
                                    )
        return redirect('secciones')
    else:
        form = SeccionForm()
        form.fields['cod_seccion'].widget = forms.HiddenInput()
    contexto = {'secciones':secciones, 'form':form, 'nbar':'Secciones', 'cursos':cursos, 'asignaturas':asignaturas, 'pfs':pfs}
    return render(request, 'SGEAPP/temp_secciones.html', contexto)

def delSeccion(request,id_seccion):
    seccion = Seccion.objects.get(id=id_seccion)
    seccion.delete()
    return redirect ('secciones')

def editSeccion(request, id_seccion):
    seccion = Seccion.objects.get(id = id_seccion)
    if request.method == 'GET':
        form = SeccionForm(instance=seccion)
    else:
        form = SeccionForm(request.POST, instance=seccion)
        if form.is_valid():
            form.save()
        return redirect('secciones')
    return render(request,'SGEAPP/editSeccion.html',{'form':form})

def v_asignaturas(request):
    asignaturas = Asignatura.objects.all()
    if request.method == 'POST':
        form = AsignaturaForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('asignaturas')
    else:
        form = AsignaturaForm()
    contexto = {'asignaturas':asignaturas, 'form':form}
    return render(request, 'SGEAPP/temp_asignaturas.html', contexto)

def delAsignatura(request,id_asignatura):
    asignatura = Asignatura.objects.get(id=id_asignatura)
    asignatura.delete()
    return redirect ('asignaturas')

def crearSeccionesBase(requests):
    cursos = Curso.objects.all()
    asignaturas = Asignatura.objects.all()
    for cur in cursos:
        for asign in asignaturas:
            Seccion.objects.create( cod_seccion= str(asign.cod_asign+cur.codigo[0:2]+'19').upper(),
                                    curso= cur,
                                    asignatura= asign,
                                    profesor= cur.profesor,
                                    )
    return redirect ('secciones')

def listNotasFilter(request, curso_id, seccion_id):
    estudiante = Estudiante.objects.filter(curso_id=curso_id)
    evaluacion = Evaluacion.objects.filter(seccion_id=seccion_id)
    seccion = Seccion.objects.all()
    list_curso = Curso.objects.all()
    nota = Nota.objects.all()
    id_cur = Curso.objects.get(id=curso_id)
    id_sec = Seccion.objects.get(id=seccion_id)
    if request.method == 'GET':
        contexto = {'estudiantes':estudiante,'list_curso':list_curso,'secciones':seccion,'evaluaciones':evaluacion,'notas':nota,'id_cur':id_cur,'id_sec':id_sec}
    else:
        idnota = request.POST.get('id')
        notanew = Nota.objects.get(id=idnota)
        form = NotaMinForm(request.POST, instance=notanew)
        if form.is_valid():
            form.save()
            contexto= {'estudiantes':estudiante,'list_curso':list_curso,'secciones':seccion,'evaluaciones':evaluacion,'notas':nota,'id_cur':id_cur,'id_sec':id_sec, 'form':form}
        else:
            idnota = request.POST.get('id')
            notanew = Nota.objects.get(id=idnota)
            form = NotaMinForm(request.POST, instance=notanew)
            contexto= {'estudiantes':estudiante,'list_curso':list_curso,'secciones':seccion,'evaluaciones':evaluacion,'notas':nota,'id_cur':id_cur,'id_sec':id_sec, 'form':form}
    return render(request, 'SGEAPP/temp_notas.html', contexto)

def v_notas(request):
    notas = Nota.objects.all()
    if request.method == 'POST':
        form = NotaForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('notas')
    else:
        form = NotaForm()
    contexto = {'notas':notas, 'form':form}
    return render(request, 'SGEAPP/temp_notasl.html', contexto)

def delNota(request,id_nota):
    nota = Nota.objects.get(id=id_nota)
    nota.delete()
    return redirect ('notas')

def ingresar_nota(request):
    if request.method == 'POST':
        form = NotaForm(request.POST)
        if form.is_valid():
            form.save()
            nota = request.POST['nota_v']
            estudiante = Estudiante.objects.get(id=request.POST['estudiante'])
            apoderado = Apoderado.objects.get(rut=estudiante.apoderado.rut)
            perfil = Perfil.objects.get(rut=apoderado.rut)
            enviarMensaje(perfil.token, "Ha sido registrada una nota " + nota + " a su hijo")
        return redirect('notas')
    else:
        form = NotaForm()
    contexto = {'form':form}
    return render(request, 'SGEAPP/ingresar_nota.html', contexto)

def crearEvaluaciones(request,id_seccion):
    seccion = Seccion.objects.get(id=id_seccion)
    estudiantes = Estudiante.objects.all().filter(curso=seccion.curso)
    newevaluacion = Evaluacion.objects.create(seccion=seccion)
    newevaluacion.save()
    for estudiante in estudiantes:
        nota = Nota.objects.create(estudiante=estudiante, evaluacion=newevaluacion, nota_v=0)
        nota.save()
        apoderado = Apoderado.objects.get(rut=estudiante.apoderado.rut)
        perfil = Perfil.objects.get(rut=apoderado.rut)
        enviarMensaje(perfil.token, "Se ha ingresado una nueva evaluación en "+str(seccion))
    return redirect('listNotasFilter', curso_id=seccion.curso.id, seccion_id=id_seccion)

def v_anotaciones(request):
    try: 
        anotaciones = Anotacion.objects.all()
        print(anotaciones)
        if request.method == 'POST':
            form = AnotacionForm(request.POST)
            print(form)
            print(form.is_valid())
            try:
                if form.is_valid():
                    form.save()
                    estudiante = Estudiante.objects.get(id=request.POST['estudiante'])
                    apoderado = Apoderado.objects.get(rut=estudiante.apoderado.rut)
                    perfil = Perfil.objects.get(rut=apoderado.rut)
                    tipoAnot = request.POST['tipo']
                    enviarMensaje(perfil.token, "Se ha registrado una anotación " + tipoAnot + " a su hijo")
                    return redirect('anotaciones')
                else:
                    raise
            except Exception as e:
                messages.error(request,str(e)) 
        else:
            form = AnotacionForm()
    except Exception as e:
        messages.error(request,str(e))
        print(e)
    contexto = {'anotaciones':anotaciones, 'form':form}
    return render(request, 'SGEAPP/temp_anotaciones.html', contexto)

def delAnotacion(request, id_anotacion):
    anotacion = Anotacion.objects.get(id=id_anotacion)
    anotacion.delete()
    return redirect ('anotaciones')

def asistencia_per(request,curso_id):
    estudiantes = Estudiante.objects.all().filter(curso_id=curso_id)
    for i in estudiantes:
        asistencia = Asistencia.objects.all().filter(estudiante=i)
        asis_p = Asistencia.objects.all().filter(estudiante=i, estado=True)
        i.asis_lenght = len(asistencia)
        i.asis_p = len(asis_p)
        try:
            i.asis_prom = round(i.asis_p*100/i.asis_lenght,2)
        except:
            i.asis_prom = 0
    contexto = {'estudiantes':estudiantes}
    return render(request,'SGEAPP/asistencia_per.html', contexto)

def informe_notas(request,curso_id,rut):
    # Obtengo un estudiante por su curso y rut
    estudiante = Estudiante.objects.get(curso_id=curso_id, rut=rut)
    # Tomo todas las secciones que pertenecen al curso
    secciones = Seccion.objects.all().filter(curso_id=curso_id)
    # Por cada sección realizo lo siguiente
    promedio_total = 0
    for i in secciones:
        # Tomo todas las evaluaciones de esa seccion
        evaluaciones = Evaluacion.objects.all().filter(seccion_id=i.id)
        # Creo un total de notas de esa seccion para el estudiante
        notas_tot = 0
        notas_cantidad = 0
        i.notas = []
        # Para completar el total de notas recorro las evaluaciones de la sección
        for x in evaluaciones:
            # Recojo la nota correspondiente a esa evaluación para ese estudiante
            try:
                nota = Nota.objects.get(evaluacion=x,estudiante=estudiante)
                i.notas.append(nota)
                notas_cantidad = notas_cantidad+1
                # Sumo la nota obtenida al total de la nota
                notas_tot = notas_tot+nota.nota_v
            except:
                False
        # Al finalizr tomo el promedio de las notas de la seción
        i.notas_cantidad = notas_cantidad
        try:
            i.promedio = notas_tot/len(evaluaciones)
        except:
            i.promedio = 0
        promedio_total = promedio_total+i.promedio
    promedio_total = round(promedio_total/len(secciones),1)
    fecha = datetime.datetime.now()
    notas = Nota.objects.filter(estudiante=estudiante)
    anotaciones = Anotacion.objects.filter(estudiante=estudiante)
    asistencia = Asistencia.objects.all().filter(estudiante=estudiante)
    asis_p = Asistencia.objects.all().filter(estudiante=estudiante, estado=True)
    estudiante.asis_lenght = len(asistencia)
    estudiante.asis_p = len(asis_p)
    try:
        estudiante.asis_prom = round(estudiante.asis_p*100/estudiante.asis_lenght,2)
    except:
        estudiante.asis_prom = 0
    contexto = {'estudiante':estudiante,'secciones':secciones,'promedio_total':promedio_total, 'fecha':fecha, 'notas':notas, 'anotaciones':anotaciones}
    return render(request,'SGEAPP/informe_notas.html', contexto)

class ServiceWorkerView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'firebase-messaging-sw.js', content_type='application/x-javascript')

@csrf_exempt
def saveToken(request):
    if request.method == 'POST':
        from fcm_django.models import FCMDevice
        currentToken = request.POST['currentToken']
        usuario = User.objects.get(id=request.POST['user'])
        perfil = Perfil.objects.get(usuario_id=usuario.id)
        perfil.token = currentToken
        perfil.save()
        device = FCMDevice()
        device.device_id = usuario.id
        device.registration_id = currentToken
        device.type = "web"
        device.name = usuario.username
        device.user = usuario
        device.save()
        # doSomething with pieFact here...
        return HttpResponse('success') # if everything is OK
        # nothing went well
    return HttpResponse('FAIL!!!!!')

def sendPushForm(request):
    return render(request, 'SGEAPP/pushReunion.html')

def sendPushReunion(request):
    head = request.POST['head']
    body = request.POST['body']
    from fcm_django.models import FCMDevice
    devices = FCMDevice.objects.all()
    try:
        devices.send_message(title=head, body=body)
        for d in devices:
            print(d.user.id)
            perf = Perfil.objects.get(usuario=d.user)
            Mensaje.objects.create(head=head,body=body, perfil=perf)
    except:
        False
    return redirect('perfil')

def enviarMensaje(currentToken, body):
    from fcm_django.models import FCMDevice
    title = "S.G.E. Notificaciones"
    try:
        perfil = Perfil.objects.get(token=currentToken)
        device = FCMDevice.objects.get(registration_id=currentToken)
        device.send_message(title=title, body=body)
        Mensaje.objects.create(head=title, body=body, perfil=perfil)
    except:
        False


#@pdf_decorator(pdfname='new_filename.pdf')
def informe_notaspdf(request,curso_id,rut):
    # Obtengo un estudiante por su curso y rut
    estudiante = Estudiante.objects.get(curso_id=curso_id, rut=rut)
    # Tomo todas las secciones que pertenecen al curso
    secciones = Seccion.objects.all().filter(curso_id=curso_id)
    # Por cada sección realizo lo siguiente
    promedio_total = 0
    for i in secciones:
        # Tomo todas las evaluaciones de esa seccion
        evaluaciones = Evaluacion.objects.all().filter(seccion_id=i.id)
        # Creo un total de notas de esa seccion para el estudiante
        notas_tot = 0
        notas_cantidad = 0
        i.notas = []
        # Para completar el total de notas recorro las evaluaciones de la sección
        for x in evaluaciones:
            # Recojo la nota correspondiente a esa evaluación para ese estudiante
            try:
                nota = Nota.objects.get(evaluacion=x,estudiante=estudiante)
                i.notas.append(nota)
                notas_cantidad = notas_cantidad+1
                # Sumo la nota obtenida al total de la nota
                notas_tot = notas_tot+nota.nota_v
            except:
                False
        # Al finalizr tomo el promedio de las notas de la seción
        i.notas_cantidad = notas_cantidad
        try:
            i.promedio = notas_tot/len(evaluaciones)
        except:
            i.promedio = 0
        promedio_total = promedio_total+i.promedio
    promedio_total = round(promedio_total/len(secciones),1)
    fecha = datetime.datetime.now()
    notas = Nota.objects.filter(estudiante=estudiante)
    anotaciones = Anotacion.objects.filter(estudiante=estudiante)
    asistencia = Asistencia.objects.all().filter(estudiante=estudiante)
    asis_p = Asistencia.objects.all().filter(estudiante=estudiante, estado=True)
    estudiante.asis_lenght = len(asistencia)
    estudiante.asis_p = len(asis_p)
    try:
        estudiante.asis_prom = round(estudiante.asis_p*100/estudiante.asis_lenght,2)
    except:
        estudiante.asis_prom = 0
    contexto = {'estudiante':estudiante,'secciones':secciones,'promedio_total':promedio_total, 'fecha':fecha, 'notas':notas, 'anotaciones':anotaciones}
    return render(request,'SGEAPP/informe_notaspdf.html', contexto)

def necesita_login(request):
    if request.user == False:
        return redirect('login_fn')