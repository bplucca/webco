from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Estudiante)
admin.site.register(Perfil)
admin.site.register(TipoPerfil)
admin.site.register(Curso)
admin.site.register(Grado)
admin.site.register(Profesor)
admin.site.register(TipoEnsenanza)
admin.site.register(Asistencia)
admin.site.register(Nota)
admin.site.register(Evaluacion)
admin.site.register(Funcionario)
admin.site.register(Apoderado)
admin.site.register(Anotacion)
admin.site.register(Seccion)
admin.site.register(Mensaje)
