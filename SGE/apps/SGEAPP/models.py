from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Perfil(models.Model):
    TIPO_USUARIO = (
        ('Profesor', 'Profesor'),
        ('Estudiante', 'Estudiante'),
        ('Funcionario', 'Funcionario'),
        ('Apoderado', 'Apoderado'),
    )
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    rut = models.CharField(max_length=20, blank=True)
    bio = models.CharField(max_length=255, blank=True)
    token = models.CharField(max_length=500, blank=True)
    tipo_usuario = models.CharField(max_length=12, choices=TIPO_USUARIO)
    foto_perfil = models.ImageField(null=True, blank=True, upload_to='uploads/thumb/', default='uploads/thumb/default.png')

    # Python 3
    def __str__(self):
        return self.usuario.username

@receiver(post_save, sender=User)
def crear_usuario_perfil(sender, instance, created, **kwargs):
    if created:
        Perfil.objects.create(usuario=instance)

@receiver(post_save, sender=User)
def guardar_usuario_perfil(sender, instance, **kwargs):
    instance.perfil.save()

class TipoPerfil(models.Model):
    nom_tipoperf = models.CharField(max_length=15, null=True)

    def __str__(self):
        return self.nom_tipoperf

class Estudiante(models.Model):
    rut = models.CharField(max_length=13, null=False, blank=False)
    nom = models.CharField(max_length=30, null=True, blank=True)
    apelp = models.CharField(max_length=20, null=True, blank=True)
    apelm = models.CharField(max_length=20, null=True, blank=True)
    curso = models.ForeignKey('Curso', models.DO_NOTHING, db_column='curso_id', null=True, blank=True)
    apoderado = models.ForeignKey('Apoderado', models.DO_NOTHING, db_column='apoderado_id', null=True, blank=True)

    def __str__(self):
      return self.nom+" "+self.apelp

class Curso(models.Model):
    codigo = models.CharField(max_length=8, null=True)
    letra = models.CharField(max_length=1, null=True)
    grado = models.ForeignKey('Grado', models.DO_NOTHING, db_column='grado', null=True)
    profesor = models.ForeignKey('Profesor',models.DO_NOTHING, db_column='profesor', null=True)

    def __str__(self):
      return str(self.codigo)

class Grado(models.Model):
    nombre = models.CharField(max_length=50, null=True, unique=True)
    tipoensenanza_cod = models.ForeignKey('TipoEnsenanza', models.DO_NOTHING, db_column='cod', null=True)

    def __str__(self):
      return self.nombre

class Profesor (models.Model):
    rut = models.CharField(max_length=13)
    nom = models.CharField(max_length=30, null=True)
    apelp = models.CharField(max_length=20, null=True)
    apelm = models.CharField(max_length=20, null=True)

    def __str__(self):
      return self.nom+" "+self.apelp
    
class Funcionario (models.Model):
    rut = models.CharField(max_length=13)
    nom = models.CharField(max_length=30, null=True)
    apelp = models.CharField(max_length=20, null=True)
    apelm = models.CharField(max_length=20, null=True)
    cargo = models.CharField(max_length=20, null=True)

    def __str__(self):
      return self.nom+" "+self.apelp

class TipoEnsenanza(models.Model):
    cod = models.IntegerField()
    nom = models.CharField(max_length=50, null=True)

    def __str__(self):
      cadena = str(self.cod)+" - "+self.nom
      return cadena

class Asistencia(models.Model):
    curso = models.ForeignKey('Curso',models.DO_NOTHING,db_column='curso',null=True)
    estudiante = models.ForeignKey('Estudiante',models.DO_NOTHING,db_column='estudiante',null=True)
    fecha = models.DateField(null=True)
    estado = models.BooleanField(default=False)

class Evaluacion(models.Model):
    nom_ev = models.CharField(max_length=15, null=False, default='Evaluacion')
    desc_ev = models.TextField(null=True, blank=True)
    seccion = models.ForeignKey('Seccion', models.DO_NOTHING, db_column='seccion', null=True, blank=True)

    def __str__(self):
      return self.nom_ev+str(self.id)

class Seccion(models.Model):
    cod_seccion = models.TextField(max_length=10, null=True, blank=True)
    curso = models.ForeignKey('Curso',models.DO_NOTHING,db_column='curso',null=True)
    asignatura = models.ForeignKey('Asignatura',models.DO_NOTHING,db_column='asignatura',null=True)
    profesor = models.ForeignKey('Profesor',models.DO_NOTHING,db_column='profesor',null=True)

    def __str__(self):
      return self.cod_seccion+" "+self.asignatura.nom_asign

class Nota(models.Model):
    nota_v = models.DecimalField(default=0.0 , null=False, max_digits=2, decimal_places=1)
    estudiante = models.ForeignKey('Estudiante',models.DO_NOTHING, db_column='estudiante', null=True)
    evaluacion = models.ForeignKey('Evaluacion',models.DO_NOTHING,db_column='evaluacion',null=True)
    fec_nota = models.DateField(null=True)

class Asignatura(models.Model):
    nom_asign = models.CharField(max_length=60, null=True)
    cod_asign = models.CharField(max_length=5, null=True)

    def __str__(self):
      return self.nom_asign

class Anotacion(models.Model):
    contenido = models.TextField(null=False)
    estudiante = models.ForeignKey('Estudiante',models.DO_NOTHING, db_column='estudiante', null=True)
    asignatura = models.ForeignKey('Asignatura',models.DO_NOTHING,db_column='asignatura',null=True)
    profesor = models.ForeignKey('Profesor',models.DO_NOTHING, db_column='profesor', null=True)
    tipo = models.CharField(max_length=10, null=True)
      
    def __str__(self):
        return self.contenido+" "+self.estudiante.nom

    def __str__(self):
        return self.tipo+" "+self.estudiante.nom

class Apoderado(models.Model):
    rut = models.CharField(max_length=13)
    nom = models.CharField(max_length=30, null=True)
    apelp = models.CharField(max_length=20, null=True)
    apelm = models.CharField(max_length=20, null=True)

    def __str__(self):
      return self.nom+" "+self.apelp

class Mensaje(models.Model):
    head = models.CharField(max_length=50)
    body = models.TextField()
    perfil = models.ForeignKey('Perfil',models.DO_NOTHING, db_column='perfil', null=True)
    leido = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

