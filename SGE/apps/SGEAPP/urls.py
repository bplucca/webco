from django.urls import path
from . import views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', login_required(views.index), name='index'),
    path('login_fn', views.login_fn, name='login_fn'),
    path('usuarios', login_required(views.usuarios), name='usuarios'),
    path('crear_cuentas_estudiantes',login_required(views.crear_cuentas_estudiantes),name="crear_cuentas_estudiantes"),
    path('crear_cuentas_profesores', login_required(views.crear_cuentas_profesores), name='crear_cuentas_profesores'),
    path('crear_cuentas_funcionarios', login_required(views.crear_cuentas_funcionarios), name='crear_cuentas_funcionarios'),
    path('delUsuario/<id_usuario>', login_required(views.delUsuario), name='delUsuario'),
    path('cleanUsuario',login_required(views.cleanUsuario), name='cleanUsuario'),
    path('perfil', login_required(views.perfil), name='perfil'),
    path('perfiles', login_required(views.perfiles), name='perfiles'),
    path('delPerfil/<id_perfil>', login_required(views.delPerfil), name='delPerfil'),
    path('editPerfil/<id_perfil>', login_required(views.editPerfil), name='editPerfil'),
    path('editUsuario/<id_usuario>', login_required(views.editUsuario), name='editUsuario'),
    path('editUser/<id_user>/',login_required(views.editUser), name='editUser'),
    path('listAsistenciaFilter/<curso_id>/<int:mes>/',login_required(views.listAsistenciaFilter),name='listAsistenciaFilter'),
    path('crearAsistenciaMes/<id_curso>/<mes>',login_required(views.crearAsistenciaMes), name='crearAsistenciaMes'),
    path('grados', login_required(views.grados), name='grados'),
    path('tiposEnsenanza', login_required(views.tiposEnsenanza), name='tiposEnsenanza'),
    path('delTipoEnsenanza/<id_tipoensenanza>/',login_required(views.delTipoEnsenanza), name='delTipoEnsenanza'),
    path('delGrado/<id_grado>/',login_required(views.delGrado), name='delGrado'),
    ################ PROFESOR ###############
    path('profesores', login_required(views.v_profesores), name='profesores'),
    path('delProfesor/<id_profesor>/',login_required(views.delProfesor), name='delProfesor'),
    path('editProfesor/<id_profesor>',login_required(views.editProfesor),name='editProfesor'),
    # ########     CURSOS   ###################
    path('cursos', login_required(views.v_cursos), name='cursos'),
    path('delCurso/<id_curso>/',login_required(views.delCurso), name='delCurso'),
    path('editCurso/<id_curso>',login_required(views.editCurso),name='editCurso'),
    ############## ESTUDIANTE #########
    path('estudiantes', login_required(views.v_estudiantes), name='estudiantes'),
    path('delEstudiante/<rut>/',login_required(views.delEstudiante), name='delEstudiante'),
    path('editEstudiante/<rut>/',login_required(views.editEstudiante), name='editEstudiante'),
    ############## SECCIONES #############
    path('secciones', login_required(views.v_secciones), name='secciones'),
    path('delSeccion/<id_seccion>/',login_required(views.delSeccion), name='delSeccion'),
    path('crearSeccionesBase',views.crearSeccionesBase,name='crearSeccionesBase'),
    path('editSeccion/<id_seccion>',views.editSeccion,name='editSeccion'),
    ######### ASIGNATURAS ############
    path('asignaturas', login_required(views.v_asignaturas), name='asignaturas'),
    path('delAsignatura/<id_asignatura>/',login_required(views.delAsignatura), name='delAsignatura'),
    ########## NOTAS #########
    path('notas',login_required(views.v_notas),name='notas'),
    path('delNota/<id_nota>/',login_required(views.delNota),name='delNota'),
    path('listNotasFilter/<curso_id>/<seccion_id>/',login_required(views.listNotasFilter),name='listNotasFilter'),
    path('ingresar_nota', login_required(views.ingresar_nota), name='ingresar_nota'),
    path('crearEvaluaciones/<id_seccion>/',login_required(views.crearEvaluaciones), name='crearEvaluaciones'),
    path('crearAsistencia/<id_curso>/<fecha>',login_required(views.crearAsistencia), name='crearAsistencia'),
    path('editAsistencia/<id_asistencia>/<id_curso>/<mes>/',login_required(views.editAsistencia), name='editAsistencia'),
    ############## ANOTACIONES ##########
    path('anotaciones', login_required(views.v_anotaciones), name='anotaciones'),
    path('delAnotacion/<id_anotacion>/',login_required(views.delAnotacion), name='delAnotacion'),
    path('asistencia_per/<curso_id>/',login_required(views.asistencia_per),name='asistencia_per'),
    path('informe_notas/<curso_id>/<rut>',login_required(views.informe_notas),name='informe_notas'),
    path('informe_notaspdf/<curso_id>/<rut>',login_required(views.informe_notaspdf),name='informe_notaspdf'),
    ############# FUNCIONARIO ###########
    path('funcionarios',login_required(views.v_funcionarios),name='funcionarios'),
    path('delFuncionario/<rut>/',login_required(views.delFuncionario), name='delFuncionario'),
    path('editFuncionario/<rut>/',login_required(views.editFuncionario), name='editFuncionario'),
    path('tipoperfiles',login_required(views.v_tipoperfiles),name='tipoperfiles'),
    path('delTipoPerfil/<id_tipoperfil>/',login_required(views.delTipoPerfil), name='delTipoPerfil'),
    path('firebase-messaging-sw.js', login_required(views.ServiceWorkerView.as_view()), name='service_worker'),
    path('saveToken', login_required(views.saveToken), name='saveToken'),
    path('sendPushForm', login_required(views.sendPushForm), name='sendPushForm'),
    path('sendPushReunion', login_required(views.sendPushReunion), name='sendPushReunion'),
    ############# APODERADO ##############
    path('apoderados',login_required(views.v_apoderados),name='apoderados'),
    path('delApoderado/<rut>/',login_required(views.delApoderado), name='delApoderado'),
    path('editApoderado/<rut>/',login_required(views.editApoderado), name='editApoderado'),

]
