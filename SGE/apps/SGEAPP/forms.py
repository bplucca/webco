from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

from .models import *

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=140, required=True)
    last_name = forms.CharField(max_length=140, required=False)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        )
        labels = {
            'username': 'Nombre de Usuario',
            'email': 'Mail',
            'first_name': 'Nombre',
            'last_name': 'Apellido',
            'password1': 'Contraseña',
            'password2': 'Confirme Contraseña',
        }

class EditUserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
        )
        labels = {
            'username': 'Nombre de Usuario',
            'email': 'Mail',
            'first_name': 'Nombre',
            'last_name': 'Apellido',
        }

class GradoForm(forms.ModelForm):

    class Meta:
        model = Grado
        fields = [
            'nombre',
            'tipoensenanza_cod',
        ]
        labels = {
            'nombre': 'Nombre',
            'tipoensenanza_cod': 'Código Tipo Enseñanza',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'tipoensenanza_cod': forms.Select(attrs={'class': 'form-control'}),
        }

class TipoEnsenanzaForm(forms.ModelForm):

    class Meta:
        model = TipoEnsenanza
        fields = [
            'cod',
            'nom',
        ]
        labels = {
            'cod': 'Código',
            'nom': 'Nombre',
        }
        widgets = {
            'cod': forms.TextInput(attrs={'class': 'form-control'}),
            'nom': forms.TextInput(attrs={'class': 'form-control'}),
        }

class ProfesorForm(forms.ModelForm):

    class Meta:
        model = Profesor
        fields = [
            'rut',
            'nom',
            'apelp',
            'apelm',
        ]
        labels = {
            'rut': 'Rut',
            'nom': 'Nombre',
            'apelp': 'Apellido Paterno',
            'apelm': 'Apellido Materno',
        }
        widgets = {
            'rut': forms.TextInput(attrs={'class': 'form-control', 'oninput':'checkRut(this)'}),
            'nom': forms.TextInput(attrs={'class': 'form-control'}),
            'apelp': forms.TextInput(attrs={'class': 'form-control'}),
            'apelm': forms.TextInput(attrs={'class': 'form-control'}),
        }

class CursoForm(forms.ModelForm):

    class Meta:
        model = Curso
        fields = [
            'codigo',
            'letra',
            'grado',
            'profesor',
        ]
        labels = {
            'codigo': 'Código',
            'letra': 'Letra',
            'grado': 'Grado',
            'profesor': 'Profesor',
        }
        widgets = {
            'codigo': forms.TextInput(attrs={'class': 'form-control'}),
            'letra': forms.TextInput(attrs={'class': 'form-control'}),
            'grado': forms.Select(attrs={'class': 'form-control'}),
            'profesor': forms.Select(attrs={'class': 'form-control'}),
        }

class EstudianteForm(forms.ModelForm):

    class Meta:
        model = Estudiante
        fields = [
            'rut',
            'nom',
            'apelp',
            'apelm',
            'curso',
            'apoderado',
        ]
        labels = {
            'rut': 'Rut',
            'nom': 'Nombre',
            'apelp': 'Apellido Paterno',
            'apelm': 'Apellido Materno',
            'curso': 'Curso',
            'apoderado': 'Apoderado',
        }
        widgets = {
            'rut': forms.TextInput(attrs={'class': 'form-control', 'oninput':'checkRut(this)'}),
            'nom': forms.TextInput(attrs={'class': 'form-control'}),
            'apelp': forms.TextInput(attrs={'class': 'form-control'}),
            'apelm': forms.TextInput(attrs={'class': 'form-control'}),
            'curso': forms.Select(attrs={'class':'form-control'}),
            'apoderado': forms.Select(attrs={'class':'form-control'}),
        }

class NotaMinForm(forms.ModelForm):

    class Meta:
        model = Nota
        fields = [
            'nota_v',
            'id',
        ]

class SeccionForm(forms.ModelForm):

    class Meta:
        model = Seccion
        fields = [
            'cod_seccion',
            'curso',
            'asignatura',
            'profesor',
        ]
        labels = {
            'cod_seccion': 'Código',
            'curso': 'Curso',
            'asignatura': 'Asignatura',
            'profesor': 'Profesor',
        }
        widgets = {
            'cod_seccion': forms.TextInput(attrs={'class': 'form-control'}),
            'curso': forms.Select(attrs={'class': 'form-control'}),
            'asignatura': forms.Select(attrs={'class':'form-control'}),
            'profesor': forms.Select(attrs={'class':'form-control'}),
        }

class AsignaturaForm(forms.ModelForm):

    class Meta:
        model = Asignatura
        fields = [
            'nom_asign',
            'cod_asign',
        ]
        labels = {
            'nom_asign': 'Nombre',
            'cod_asign': 'Código Asignatura',
        }
        widgets = {
            'nom_asign': forms.TextInput(attrs={'class': 'form-control'}),
            'cod_asign': forms.TextInput(attrs={'class': 'form-control'}),
        }

class AsistenciaForm(forms.ModelForm):

    class Meta:
        model = Asistencia
        fields = [
            'curso',
            'estudiante',
            'fecha',
            'estado',
        ]
        labels = {
            'curso': 'Curso',
            'estudiante': 'Estudiante',
            'fecha': 'Fecha',
            'estado': 'Presente',
        }
        widgets = {
            'curso': forms.Select(attrs={'class': 'form-control'}),
            'estudiante': forms.Select(attrs={'class': 'form-control'}),
            'fecha': forms.TextInput(attrs={'id':'datepicker'}),
        }

class AsistenciaMinForm(forms.ModelForm):

    class Meta:
        model = Asistencia
        fields = [
            'estado',
        ]

class AnotacionForm(forms.ModelForm):

    class Meta:
        model = Anotacion
        fields = [
            'contenido',
            'estudiante',
            'asignatura',
            'profesor',
            'tipo',
        ]
        labels = {
            'contenido': 'Contenido',
            'estudiante': 'Estudiante',
            'asignatura': 'Asignatura',
            'profesor': 'Profesor',
            'tipo': 'Tipo Anotacion',
        }
        widgets = {
            'contenido': forms.TextInput(attrs={'class': 'form-control'}),
            'estudiante': forms.Select(attrs={'class': 'form-control'}),
            'asignatura': forms.Select(attrs={'class': 'form-control'}),
            'profesor': forms.Select(attrs={'class': 'form-control'}),
            'tipo': forms.TextInput(attrs={'class': 'form-control'})
        }

class FuncionarioForm(forms.ModelForm):

    class Meta:
        model = Funcionario
        fields = [
            'rut',
            'nom',
            'apelp',
            'apelm',
            'cargo',
        ]
        labels = {
            'rut': 'RUT',
            'nom': 'NOMBRE',
            'apelp': 'APELLIDO PATERNO',
            'apelm': 'APELLIDO MATERNO',
            'cargo': 'CARGO',
        }
        widgets = {
            'rut': forms.TextInput(attrs={'class': 'form-control', 'oninput':'checkRut(this)'}),
            'nom': forms.TextInput(attrs={'class': 'form-control'}),
            'apelp': forms.TextInput(attrs={'class': 'form-control'}),
            'apelm': forms.TextInput(attrs={'class': 'form-control'}),
            'cargo': forms.TextInput(attrs={'class': 'form-control'})
        }

class TipoPerfilForm(forms.ModelForm):

    class Meta:
        model = TipoPerfil
        fields = [
            'nom_tipoperf',
        ]
        labels = {
            'nom_tipoperf':'Nombre Perfil',
        }
        widgets = {
            'nom_tipoperf': forms.TextInput(attrs={'class': 'form-control'}),
        }

class ApoderadoForm(forms.ModelForm):

    class Meta:
        model = Apoderado
        fields = [
            'rut',
            'nom',
            'apelp',
            'apelm',
        ]
        labels = {
            'rut': 'RUT',
            'nom': 'NOMBRE',
            'apelp': 'APELLIDO PATERNO',
            'apelm': 'APELLIDO MATERNO',
        }
        widgets = {
            'rut': forms.TextInput(attrs={'class': 'form-control', 'oninput':'checkRut(this)'}),
            'nom': forms.TextInput(attrs={'class': 'form-control'}),
            'apelp': forms.TextInput(attrs={'class': 'form-control'}),
            'apelm': forms.TextInput(attrs={'class': 'form-control'}),
        }

class PerfilForm(forms.ModelForm):

    class Meta:
        model = Perfil
        fields = [
            'usuario',
            'rut',
            'bio',
            'tipo_usuario',
            'foto_perfil',
        ]
        labels = {
            'usuario': 'Usuario',
            'rut': 'RUT',
            'bio': 'BIO',
            'tipo_usuario': 'Tipo Usuario',
            'foto_perfil': 'Foto Perfil',
        }

class UsuarioForm(forms.ModelForm):
    first_name = forms.CharField(max_length=140, required=True)
    last_name = forms.CharField(max_length=140, required=False)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
        )
        labels = {
            'username': 'Nombre de Usuario',
            'email': 'Mail',
            'first_name': 'Nombre',
            'last_name': 'Apellido',
        }

class NotaForm(forms.ModelForm):
    
    class Meta:
        model = Nota
        fields = [
            'nota_v',
            'estudiante',
            'evaluacion',
            'fec_nota',
        ]
        labels = {
            'nota_v': 'Nota',
            'estudiante': 'Estudiante',
            'evaluacion': 'Evaluación',
            'fec_nota': 'Fecha',
        }
        widgets = {
            'nota_v': forms.TextInput(attrs={'class': 'form-control'}),
            'estudiante': forms.Select(attrs={'class': 'form-control'}),
            'evaluacion': forms.Select(attrs={'class': 'form-control'}),
            'fec_nota': forms.TextInput(attrs={'class': 'form-control','id':'datepicker'}),
        }